import java.util.Scanner;

public class Roulette {
    public static void main(String[] args) {
        //creating a scanner input
        Scanner input=new Scanner(System.in);
        //creating a RouletteWheel object
        RouletteWheel wheel=new RouletteWheel();

        System.out.println("Welcome to a game of Roulette!");
        //initializing the total win
        int totalWin=0;
        System.out.println("Would you like to bet? If so, which number in the wheel would you like to bet?");
        int betNum=input.nextInt();

        System.out.println("How much would you like to bet?");
        int betMoney=input.nextInt();

        //spinning the wheel
        wheel.spin();

        if (wheel.getValue()==betNum) {
            System.out.println("Congratulations! You won!");
            totalWin+=betMoney*(betNum-1);
        }
        else {
            System.out.println("Too bad! you lost "+betMoney+"$");
            totalWin=totalWin-betMoney;
        }
        System.out.println("Would you like to play again?(1) for yes and (2) for no");
        int playAgain=input.nextInt();

        //adding a loop if player wants to play again
        while (playAgain != 2) {
            System.out.println("Which number would you like to bet?");
            betNum=input.nextInt();
            System.out.println("How much would you like to bet?");
            betMoney=input.nextInt();

            wheel.spin();

            if (wheel.getValue()==betNum) {
                System.out.println("Congratulations! You won!");
                totalWin+=betMoney*(betNum-1);
            }
            else {
                System.out.println("Too bad! you lost "+betMoney+"$");
                totalWin=totalWin-betMoney;
            }
            System.out.println("would you like to play again?(1) for yes and (2) for no");
            playAgain=input.nextInt();
        }
        System.out.println("Thank you for playing roulette! Your total win is "+totalWin+"$");
    }
}
