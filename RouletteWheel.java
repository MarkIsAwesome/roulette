import java.util.Random;
public class RouletteWheel {
    private Random randNum;
    private int lastSpin;

    public RouletteWheel() {
        this.randNum=new Random();
        this.lastSpin=0;
    }

    //adding a method called spin
    public void spin() {
        this.lastSpin=this.randNum.nextInt(37);
    }

    //adding a new method called get Value
    public int getValue() {
        return this.lastSpin;
    }
    
}